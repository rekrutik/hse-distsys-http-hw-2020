import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_empty_modify():
    response = requests.patch(URL + '/api/v1.0/modify_entry')
    assert response.status_code == 400
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={})
    assert response.status_code == 400
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'title': 'zxc'})
    assert response.status_code == 400
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'id': 'ololo', 'title': 'zxc'})
    assert response.status_code == 400

    entry = {'title': 'zxc', 'contents': 'clown'}
    response = requests.post(URL + '/api/v1.0/add_entry', json=entry)
    assert response.status_code == 201
    assert response.json() != None
    assert 'id' in response.json()
    assert type(response.json()['id']) is int
    entry_id = response.json()['id']

    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'id': entry_id + 1, 'title': 'zxc'})
    assert response.status_code == 404
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'id': entry_id, 'title': 123})
    assert response.status_code == 400
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'id': entry_id, 'contents': 123})
    assert response.status_code == 400
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'id': entry_id, 'title': 'zxc', 'contents': 123})
    assert response.status_code == 400
    response = requests.patch(URL + '/api/v1.0/modify_entry', json={'id': entry_id, 'title': 123, 'contents': 'zxc'})
    assert response.status_code == 400