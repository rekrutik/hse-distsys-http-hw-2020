import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_multi():
    entries = [
        {'title': 'zxc', 'contents': 'clown'},
        {'title': 'asd', 'contents': 'asdas'},
        {'title': 'qwe', 'contents': 'asda'},
        {'title': 'tyu', 'contents': 'cldsdsown'},
        {'title': 'uyt', 'contents': 'dsadsa'}]
    min_id = 1e9
    for e in entries:
        response = requests.post(URL + '/api/v1.0/add_entry', json=e)
        assert response.status_code == 201
        assert response.json() != None
        assert 'id' in response.json()
        assert type(response.json()['id']) is int
        min_id = min(min_id, response.json()['id'])
    response = requests.get(URL + '/api/v1.0/get_entries_list', json={'offset': min_id, 'count': 5})
    assert response.status_code == 200
    assert type(response.json()['entries']) is list
    for i, e in enumerate(response.json()['entries']):
        assert e['title'] == entries[i]['title']
        assert e['contents'] == entries[i]['contents']
    response = requests.get(URL + '/api/v1.0/get_entries_list', json={'offset': min_id + 4, 'count': 5, 'order': 'DESC'})
    print(response)
    assert response.status_code == 200
    assert type(response.json()['entries']) is list
    for i, e in enumerate(response.json()['entries']):
        assert e['title'] == entries[::-1][i]['title']
        assert e['contents'] == entries[::-1][i]['contents']
    response = requests.get(URL + '/api/v1.0/get_entries_list', json={'offset': min_id + 2, 'count': 3})
    assert response.status_code == 200
    assert type(response.json()['entries']) is list
    for i, e in enumerate(response.json()['entries']):
        assert e['title'] == entries[2:][i]['title']
        assert e['contents'] == entries[2:][i]['contents']
    response = requests.get(URL + '/api/v1.0/get_entries_list', json={'offset': min_id + 2, 'count': 3, 'order': 'DESC'})
    assert response.status_code == 200
    assert type(response.json()['entries']) is list
    for i, e in enumerate(response.json()['entries']):
        assert e['title'] == entries[2::-1][i]['title']
        assert e['contents'] == entries[2::-1][i]['contents']