import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_empty_add():
    response = requests.post(URL + '/api/v1.0/add_entry')
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={})
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={'title': 'zxc'})
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={'contents': 'clown'})
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={'title': 123})
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={'contents': 123})
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={'title': 'zxc', 'contents': 123})
    assert response.status_code == 400
    response = requests.post(URL + '/api/v1.0/add_entry', json={'title': 123, 'contents': 'zxc'})
    assert response.status_code == 400