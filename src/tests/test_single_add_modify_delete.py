import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_single_add_modify_delete():
    entry = {'title': 'zxc', 'contents': 'clown'}
    modified_entry = {'title': 'asd', 'contents': 'qwe'}
    response = requests.post(URL + '/api/v1.0/add_entry', json=entry)
    assert response.status_code == 201
    assert response.json() != None
    assert 'id' in response.json()
    assert type(response.json()['id']) is int
    entry_id = response.json()['id']
    response = requests.get(URL + '/api/v1.0/get_entry', json={'id': entry_id})
    assert response.status_code == 200
    assert response.json()['id'] == entry_id
    assert response.json()['title'] == entry['title']
    assert response.json()['contents'] == entry['contents']
    response = requests.patch(URL + '/api/v1.0/modify_entry',
        json={'id': entry_id, 'title': modified_entry['title'], 'contents': modified_entry['contents']})
    assert response.status_code == 204
    response = requests.get(URL + '/api/v1.0/get_entry', json={'id': entry_id})
    assert response.status_code == 200
    assert response.json()['id'] == entry_id
    assert response.json()['title'] == modified_entry['title']
    assert response.json()['contents'] == modified_entry['contents']
    response = requests.delete(URL + '/api/v1.0/delete_entry', json={'id': entry_id})
    assert response.status_code == 204
    response = requests.get(URL + '/api/v1.0/get_entry', json={'id': entry_id})
    assert response.status_code == 404