import os

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)


def test_empty_delete():
    response = requests.delete(URL + '/api/v1.0/delete_entry')
    assert response.status_code == 400
    response = requests.delete(URL + '/api/v1.0/delete_entry', json={})
    assert response.status_code == 400
    response = requests.delete(URL + '/api/v1.0/delete_entry', json={'id': 228})
    assert response.status_code == 404
    response = requests.delete(URL + '/api/v1.0/delete_entry', json={'id': 'ololo'})
    assert response.status_code == 400