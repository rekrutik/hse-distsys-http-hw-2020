import os
from bisect import bisect_left

from flask import Flask, make_response, abort, request, jsonify
from werkzeug.exceptions import HTTPException

app = Flask(__name__)

entries = list()
entries_index = dict()
last_id = 0

@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    text = 'Internal server error'
    if isinstance(e, HTTPException):
        code = e.code
        text = e.description
    else:
        app.logger.debug('[Exception] {}'.format(e))
    return jsonify(error=text), code

@app.route('/api/v1.0/add_entry', methods=['POST'])
def add_entry():
    global last_id
    if not request.json:
        abort(400, description='Input is not JSON')
    if 'title' not in request.json:
        abort(400, description='title parameter must be specified')
    if type(request.json['title']) is not str:
        abort(400, description='title parameter must be a string')
    if 'contents' not in request.json:
        abort(400, description='contents parameter must be specified')
    if type(request.json['contents']) is not str:
        abort(400, description='contents parameter must be a string')
    entry_id = last_id + 1
    entry = {
        'id': entry_id,
        'title': request.json['title'],
        'contents': request.json['contents']
    }
    entries.append(entry_id)
    entries_index[entry_id] = entry
    last_id += 1
    return jsonify({'id': entry['id']}), 201

@app.route('/api/v1.0/get_entry', methods=['GET'])
def get_entry():
    if not request.json:
        abort(400, description='Input is not JSON')
    if 'id' not in request.json:
        abort(400, description='id parameter must be specified')
    if type(request.json['id']) is not int:
        abort(400, description='id parameter must be an integer')
    if request.json['id'] not in entries_index:
        abort(404, description='entry with specified id has not been found')
    return jsonify(entries_index[request.json['id']]), 200

@app.route('/api/v1.0/get_entries_list', methods=['GET'])
def get_entries_list():
    if not request.json:
        abort(400, description='Input is not JSON')
    offset = 1
    count = 10
    asc = True
    if 'offset' in request.json:
        if type(request.json['offset']) is not int:
            abort(400, description='offset parameter must be an integer')
        if request.json['offset'] < 0:
            abort(400, description='offset can\'t be negative')
        offset = request.json['offset']
    if 'count' in request.json:
        if type(request.json['count']) is not int:
            abort(400, description='count parameter must be an integer')
        if request.json['count'] < 0:
            abort(400, description='count can\'t be negative')
        count = request.json['count']
    if 'order' in request.json:
        if not (request.json['order'] == 'ASC' or request.json['order'] == 'DESC'):
            abort(400, description='order must have either ASC or DESC value')
        asc = request.json['order'] == 'ASC'
    res_ids = None
    pos = bisect_left(entries, request.json['offset'])
    if asc:
        res_ids = entries[pos:min(len(entries), pos + count)]
    else:
        res_ids = entries[pos:max(0, pos - count):-1]
    return jsonify(entries=list(map(lambda x: entries_index[x], res_ids))), 200
    
@app.route('/api/v1.0/delete_entry', methods=['DELETE'])
def delete_entry():
    if not request.json:
        abort(400, description='Input is not JSON')
    if 'id' not in request.json:
        abort(400, description='id parameter must be specified')
    if type(request.json['id']) is not int:
        abort(400, description='id parameter must be an integer')
    if request.json['id'] not in entries_index:
        abort(404, description='entry with specified id has not been found')
    entries.remove(request.json['id'])
    entries_index.pop(request.json['id'], None)
    return '', 204

@app.route('/api/v1.0/modify_entry', methods=['PATCH'])
def modify_entry():
    if not request.json:
        abort(400, description='Input is not JSON')
    if 'id' not in request.json:
        abort(400, description='id parameter must be specified')
    if type(request.json['id']) is not int:
        abort(400, description='id parameter must be an integer')
    if request.json['id'] not in entries_index:
        abort(404, description='entry with specified id has not been found')
    title = entries_index[request.json['id']]['title']
    contents = entries_index[request.json['id']]['contents']
    if 'title' in request.json:
        if type(request.json['title']) is not str:
            abort(400, description='title parameter must be a string')
        title = request.json['title']
    if 'contents' in request.json:
        if type(request.json['contents']) is not str:
            abort(400, description='contents parameter must be a string')
        contents = request.json['contents']
    entry = {
        'id': request.json['id'],
        'title': title,
        'contents': contents
    }
    entries_index[request.json['id']] = entry
    return '', 204

app.debug = True
app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
